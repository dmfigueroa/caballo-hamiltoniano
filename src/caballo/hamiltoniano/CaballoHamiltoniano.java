/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballo.hamiltoniano;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author dmfigueroa
 */
public class CaballoHamiltoniano {

    static int[][] tablero;

    private static final Coordenada[] MOVIMIENTOS_POSIBLES = new Coordenada[]{
        new Coordenada(2, 1),
        new Coordenada(1, 2),
        new Coordenada(-1, 2),
        new Coordenada(-2, 1),
        new Coordenada(-2, -1),
        new Coordenada(-1, -2),
        new Coordenada(1, -2),
        new Coordenada(2, -1),};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el tamaño del tablero");
        int n = sc.nextInt();
        sc.nextLine();
        tablero = new int[n][n];
        System.out.println("Digite la posición inicial del caballo: (Desde 1 hasta n, así \"(3,4)\") ");
        String[] input = sc.nextLine().split(",");
        int fila = Integer.parseInt(input[0]) - 1;
        int columna = Integer.parseInt(input[1]) - 1;
        if (caballoSolución(fila, columna, 1)) {
            imprimirTablero(tablero);
        }
    }

    private static boolean caballoSolución(int x, int y, int movimientoActual) {
        if (movimientoActual == tablero.length * tablero.length) {
            tablero[y][x] = movimientoActual;
            return true;
        }
        tablero[y][x] = movimientoActual;
        for (Coordenada c : obtenerMovimientos(x, y)) {
            if (legal(c.getX(), c.getY())) {
                if (caballoSolución(c.getX(), c.getY(), movimientoActual + 1)) {
                    return true;
                }
            }
        }
        tablero[y][x] = 0;
        return false;
    }

    private static void imprimirTablero(int[][] tablero) {
        for (int[] tablero1 : tablero) {
            for (int j = 0; j < tablero.length; j++) {
                System.out.print(tablero1[j] + "	");
            }
            System.out.println(" ");
        }
    }

    private static ArrayList<Coordenada> obtenerMovimientos(int x, int y) {
        ArrayList<Coordenada> movimientos = new ArrayList<>();
        for (Coordenada posible : MOVIMIENTOS_POSIBLES) {
            int cont = 0;
            if (legal(posible.getX() + x, posible.getY() + y)) {
                for (Coordenada coordenada : MOVIMIENTOS_POSIBLES) {
                    if (legal(posible.getX() + coordenada.getX() + x, posible.getY() + coordenada.getY() + y)) {
                        cont++;
                    }
                }
            }
            movimientos.add(new Coordenada(posible.getX() + x, posible.getY() + y, cont));
        }
        Collections.sort(movimientos, (Coordenada c1, Coordenada c2) -> Integer.compare(c1.getMovimientos(), c2.getMovimientos()));
        return movimientos;
    }

    private static boolean legal(int x, int y) {
        return (0 <= y && y <= tablero.length - 1 && 0 <= x && x <= tablero.length - 1 && tablero[y][x] == 0);
    }

}
