/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballo.hamiltoniano;

/**
 *
 * @author David
 */
public class Coordenada {

    private int x;
    private int y;
    private int movimientos;

    public Coordenada(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordenada(int x, int y, int moviemientos) {
        this.movimientos = moviemientos;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getMovimientos() {
        return movimientos;
    }
}
